package control;

import java.util.ArrayList;
import java.util.Collections;

import Interface.Taxable;
import model.Company;
import model.EarningComparator;
import model.ExpenseComparator;
import model.Person;
import model.Product;
import model.ProfitComparator;
import model.TaxableComparator;
import viewer.Gui;

public class Controller {
	private Gui frame;
	private ArrayList<Person> personList = new ArrayList<Person>();
	private ArrayList<Product> productList = new ArrayList<Product>();
	private ArrayList<Company> companyList = new ArrayList<Company>();
	private ArrayList<Taxable> taxList = new ArrayList<Taxable>();
	
	public Controller() {
		// TODO Auto-generated constructor stub
		frame = new Gui();
		frame.setSize(400, 500);
		frame.setVisible(true);
		frame.setLocation(200, 100);
		testCase();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();

	}
	public void testCase(){
		frame.setTextResults("------------------ Person -------------------");
		personList.add(new Person("Boom", 230000));
		personList.add(new Person("Pond", 123000));
		personList.add(new Person("Wut", 150000));
		for(Person persons : personList){
			frame.setTextResults("name :"+persons.getName()+" Salary :"+persons.getSalary());
		}
		frame.setTextResults("--------- After Sort --------------");
		Collections.sort(personList);
		for(Person persons : personList){
			frame.setTextResults("name :"+persons.getName()+" Salary :"+persons.getSalary());
		}
		frame.setTextResults("\n------------------ Product -------------------");
		productList.add(new Product("Phone", 24000));
		productList.add(new Product("Bag", 12000));
		productList.add(new Product("Belt", 32000));
		for(Product products : productList){
			frame.setTextResults("name :"+products.getName()+" Price :"+products.getPrice());
		}
		frame.setTextResults("--------------------- After Sort -------------------");
		Collections.sort(productList);
		for(Product products : productList){
			frame.setTextResults("name :"+products.getName()+" Price :"+products.getPrice());
		}
		frame.setTextResults("\n--------------------- Company ------------------------");
		companyList.add(new Company("Wut Ltd.", 3200000, 1400000));
		companyList.add(new Company("Boom Ltd.", 400000, 123000));
		companyList.add(new Company("Pond Ltd.", 2000000, 803000));
		for(Company companys : companyList){
			frame.setTextResults("name :"+companys.getName()+" Income :"+companys.getIncome()+" Expenses :"+companys.getExpenses());
		}
		frame.setTextResults("-------------------------- After Sort Income -----------------------");
		Collections.sort(companyList, new EarningComparator());
		for(Company companys : companyList){
			frame.setTextResults("name :"+companys.getName()+" Income :"+companys.getIncome()+" Expenses :"+companys.getExpenses());
		}
		frame.setTextResults("-------------------------- After Sort Expenses -----------------------");
		Collections.sort(companyList,new ExpenseComparator());
		for(Company companys : companyList){
			frame.setTextResults("name :"+companys.getName()+" Expenses :"+companys.getExpenses()+" Income :"+companys.getIncome());
		}
		frame.setTextResults("------------------------------ Profit ------------------------------");
		for(Company companys : companyList){
			frame.setTextResults("name :"+companys.getName()+" Profit :"+companys.getTax());
		}
		frame.setTextResults("------------------------------ After Sort Profit -------------------------");
		Collections.sort(companyList,new ProfitComparator());
		for(Company companys : companyList){
			frame.setTextResults("name :"+companys.getName()+" Profit :"+companys.getTax());
		}
		frame.setTextResults("\n---------- Person Product Company -------------------");
		taxList.add(new Person("Wut", 21329));
		taxList.add(new Product("Pond", 7893));
		taxList.add(new Company("Boom Ltd.", 1200000, 239000));
		for(Taxable taxs : taxList){
			frame.setTextResults("tax :"+taxs.getTax());
		}
		frame.setTextResults("-------------------------- After Sort Tax ---------------------");
		Collections.sort(taxList,new TaxableComparator());
		for(Taxable taxs : taxList){
			frame.setTextResults("tax :"+taxs.getTax());
		}
		
	}
	

}
