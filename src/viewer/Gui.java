package viewer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Gui extends JFrame {

	private JTextArea resultsA;
	public Gui() {
		// TODO Auto-generated constructor stub
		createFrame();
	}
	public void createFrame(){
		resultsA = new JTextArea("Results");
		JScrollPane scrollResult = new JScrollPane(resultsA);
		add(scrollResult);
	}
	public void setTextResults(String str){
		resultsA.setText(resultsA.getText()+"\n"+str);
	}

}
