package model;

import Interface.Taxable;

public class Company implements Comparable<Company> , Taxable {
	private String name;
	private double income;
	private double expenses;
	public Company(String name,double income,double expenses){
		this.name = name;
		this.income = income;
		this.expenses = expenses;
	}
	public String getName(){
		return this.name;
	}
	public double getIncome(){
		return income;
	}
	public double getExpenses(){
		return expenses;
	}

	@Override
	public int compareTo(Company other) {
		// TODO Auto-generated method stub
		if(this.getIncome()<other.getIncome()){
			return -1;
		}
		else if(this.getIncome()<other.getIncome()){
			return 1;
		}
		return 0;
	}
	@Override
	public double getTax() {
		return 0.3 * (this.getIncome() - this.getExpenses());
	}

}
