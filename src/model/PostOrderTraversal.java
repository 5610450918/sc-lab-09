package model;

import java.awt.List;
import java.util.ArrayList;

public class PostOrderTraversal implements Traversal {

	@Override
	public ArrayList<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		ArrayList<Node> tree;
		if(node==null){
			return new ArrayList<Node>();
		}
		else{
			tree= traverse(node.getLeft());
			tree.addAll(traverse(node.getRight()));
			tree.add(node);
			return tree;
		}
	}

}
