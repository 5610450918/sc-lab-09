package model;

import Interface.Taxable;

public class Product implements Comparable<Product> ,Taxable {
	private String name;
	private double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}
	@Override
	public int compareTo(Product other) {
		// TODO Auto-generated method stub
		if(this.getPrice()<other.getPrice()){
			return -1;
		}
		else if(this.getPrice()>other.getPrice()){
			return 1;
		}
		return 0;
	}

	@Override
	public double getTax() {
		return 0.07 * this.getPrice();
	}
	

}
