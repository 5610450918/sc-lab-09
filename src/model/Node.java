package model;

public class Node {
	private String name;
	private Node left;
	private Node right;
	
	public Node(String name,Node left,Node right) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.left = left;
		this.right = right;
	}
	public String getName(){
		return this.name;
	}
	public Node getLeft(){
		return this.left;
	}
	public Node getRight(){
		return this.right;
	}

}
