package model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxableComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable t1, Taxable t2) {
		// TODO Auto-generated method stub
		double i1 = t1.getTax();
		double i2 = t2.getTax();
		if(i1<i2){
			return 1;
		}
		else if(i1>i2){
			return -1;
		}
		return 0;
	}

}
