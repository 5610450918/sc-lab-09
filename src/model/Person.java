package model;

import Interface.Taxable;


public class Person implements Comparable<Person>,Taxable {
	private String name;
	private int salary;
	public Person(String name,int salary){
		this.name = name;
		this.salary = salary;
	}
	public String getName(){
		return name;
	}
	public int getSalary(){
		return salary;
	}
	@Override
	public int compareTo(Person other) {
		// TODO Auto-generated method stub
		if(other.getSalary()>this.getSalary()){
			return -1;
		}
		else if(other.getSalary()<this.getSalary()){
			return 1;
		}
		return 0;
		
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		if(salary<=300000){
			return salary*0.05;
		}
		else{
			double tax = salary-300000;
			tax = tax*0.1;
			return tax+(300000*0.05);
		}
	}
	
	

}
