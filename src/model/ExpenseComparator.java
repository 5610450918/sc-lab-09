package model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company c1, Company c2) {
		// TODO Auto-generated method stub
		double i1 = c1.getExpenses();
		double i2 = c2.getExpenses();
		if(i1<i2){
			return -1;
		}
		if(i1>i2){
			return 1;
		}
		return 0;
	}

}
